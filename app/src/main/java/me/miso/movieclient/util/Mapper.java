package me.miso.movieclient.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.common.collect.Lists;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import me.miso.movieclient.greendao.dao.MovieDetail;
import me.miso.movieclient.greendao.dao.MovieListItem;
import me.miso.movieclient.greendao.dao.MovieSearch;

/**
 * Created by miso on 08.10.2015.
 */
public class Mapper {

    public static MovieSearch movieSearchMapToMovieSearchDB(me.miso.movieclient.model.MovieSearch movieSearch) {
        return new MovieSearch(movieSearch.getId(), movieSearch.getQueryText());
    }

    public static me.miso.movieclient.model.MovieSearch movieSearchDBMapToMovieSearch(MovieSearch movieSearchDB) {
        me.miso.movieclient.model.MovieSearch movieSearch = new me.miso.movieclient.model.MovieSearch();
        movieSearch.setQueryText(movieSearchDB.getQueryText());
        movieSearch.setId(movieSearchDB.getId());
        List<me.miso.movieclient.model.MovieListItem> list = Lists.newArrayList();
        for (MovieListItem listItem : movieSearchDB.getMovieListItemList()) {
            list.add(movieListItemDBMapToMovieListItem(listItem));
        }
        movieSearch.setSearch(list);
        return movieSearch;
    }

    public static MovieListItem movieListItemMapToMovieListItemDB(me.miso.movieclient.model.MovieListItem movieListItem, long searchId) {
        return new MovieListItem(movieListItem.getTitle(), movieListItem.getYear(), movieListItem.getImdbID(), movieListItem.getType(), searchId);
    }

    public static me.miso.movieclient.model.MovieListItem movieListItemDBMapToMovieListItem(MovieListItem movieListItemDB) {
        me.miso.movieclient.model.MovieListItem movieListItem = new me.miso.movieclient.model.MovieListItem();
        movieListItem.setImdbID(movieListItemDB.getImdbID());
        movieListItem.setTitle(movieListItemDB.getTitle());
        movieListItem.setType(movieListItemDB.getType());
        movieListItem.setYear(movieListItemDB.getYear());
        return movieListItem;
    }

    public static MovieDetail movieDetailMapToMovieDetailDB(me.miso.movieclient.model.MovieDetail movieDetail) {
        return new MovieDetail(movieDetail.getId(), movieDetail.getTitle(), movieDetail.getYear(), movieDetail.getRuntime(), bitmapToByteArray(movieDetail.getBitmapPoster()), movieDetail.getImdbID());
    }

    public static me.miso.movieclient.model.MovieDetail movieDetailDBMapToMovieDetail(MovieDetail movieDetailDB) {
        me.miso.movieclient.model.MovieDetail movieDetail = new me.miso.movieclient.model.MovieDetail();
        movieDetail.setTitle(movieDetailDB.getTitle());
        movieDetail.setYear(movieDetailDB.getRelease());
        movieDetail.setRuntime(movieDetailDB.getRuntime());
        movieDetail.setId(movieDetailDB.getId());
        movieDetail.setBitmapPoster(byteArrayToBitmap(movieDetailDB.getPoster()));
        movieDetail.setImdbID(movieDetailDB.getImdbID());
        return movieDetail;
    }

    private static Bitmap byteArrayToBitmap(byte[] poster) {
        if (poster != null) {
            return BitmapFactory.decodeByteArray(poster, 0, poster.length);
        }
        return null;
    }


    private static byte[] bitmapToByteArray(Bitmap poster) {
        if (poster != null) {
            ByteArrayOutputStream stream;
            stream = new ByteArrayOutputStream();
            poster.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] bytes = stream.toByteArray();
            try {
                stream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return bytes;
        }
        return null;
    }
}
