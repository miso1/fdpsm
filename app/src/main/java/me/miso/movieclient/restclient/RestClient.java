package me.miso.movieclient.restclient;

import android.graphics.Bitmap;
import android.graphics.Movie;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.common.collect.Maps;
import com.google.gson.Gson;

import java.util.List;
import java.util.Map;

import me.miso.movieclient.MovieApplication;
import me.miso.movieclient.model.MovieDetail;
import me.miso.movieclient.model.MovieListItem;
import me.miso.movieclient.model.MovieSearch;

/**
 * Created by miso on 18.09.2015.
 */
public enum RestClient {

    INSTANCE;

    private RequestQueue queue;
    private Uri uri;

    RestClient() {
        queue = Volley.newRequestQueue(MovieApplication.getContext());
        uri = Uri.parse("http://www.omdbapi.com/?");
    }

    public static RestClient getInstance() {
        return INSTANCE;
    }

    public RestRequest<MovieSearch> searchMovies(String searchTerm) {
        return new RestRequest<>(uri, MovieSearch.class, queue).addParam("s", searchTerm).withMethod(Request.Method.GET);
        /*String url = uri
                .buildUpon()
                .appendQueryParameter("s", searchTerm)
                .build().toString();

        // Request a string response_list from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response_list) {
                    // Display the first 500 characters of the response_list string.
                    MovieSearch list = new Gson().fromJson(response_list, MovieSearch.class);
                    listener.onResponse(list.getSearch());
                }
            }, errorListener
        );
        // Add the request to the RequestQueue.
        queue.add(stringRequest);*/
    }

    public RestRequest<MovieDetail> getMovieDetailByImdbID(String imdbID) {
        return new RestRequest<>(uri, MovieDetail.class, queue).addParam("i", imdbID).withMethod(Request.Method.GET);
        /*String url = uri
                .buildUpon()
                .appendQueryParameter("i", imdbID)
                .build().toString();

        // Request a string response_list from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response_list) {
                        // Display the first 500 characters of the response_list string.
                        MovieDetail detail = new Gson().fromJson(response_list, MovieDetail.class);
                        listener.onResponse(detail);
                    }
                }, errorListener
        );
        // Add the request to the RequestQueue.
        queue.add(stringRequest);*/
    }

    public void getBitmapFromURL(String url, final Response.Listener<Bitmap> listener, final Response.ErrorListener errorListener) {
        ImageRequest imageRequest = new ImageRequest(url, listener, 300, 300, ImageView.ScaleType.CENTER, Bitmap.Config.RGB_565, errorListener);
        queue.add(imageRequest);
    }

    public interface Listener<T> {
        void onResponse(T response);
    }

    public static class RestRequest<T> {
        private Uri.Builder uriBuilder;
        private Class<T> returnClass;
        private RequestQueue queue;
        private int method = Request.Method.GET;
        private String url;
        private Listener<T> onSuccessListener;

        private RestRequest(Uri baseUri, Class<T> returnClass, RequestQueue queue) {
            this.queue = queue;
            this.uriBuilder = baseUri.buildUpon();
            this.returnClass = returnClass;
        }

        public RestRequest<T> addParam(String key, String value) {
            uriBuilder.appendQueryParameter(key, value);
            return this;
        }

        public RestRequest<T> withMethod(int httpMethod) {
            this.method = httpMethod;
            return this;
        }

        public RestRequest<T> onSuccess(Listener<T> onSuccessListener) {
            this.onSuccessListener = onSuccessListener;
            return this;
        }

        //TODO onError

        public void send() {
            String uriString = uriBuilder.build().toString();
            StringRequest stringRequest = new StringRequest(method, uriString,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            T responseObject = new Gson().fromJson(response, returnClass);
                            onSuccessListener.onResponse(responseObject);
                        }
                    }, null);
            queue.add(stringRequest);
        }
    }
}
