package me.miso.movieclient.restclient;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import me.miso.movieclient.model.MovieDetail;
import me.miso.movieclient.model.MovieSearch;

/**
 * Created by miso on 29.09.2015.
 */

@Rest(rootUrl = "http://www.omdbapi.com/?",converters = { MappingJackson2HttpMessageConverter.class })
public interface MovieClient {

    @Get("s={searchTerm}")
    MovieSearch searchMovies(String searchTerm);

    @Get("i={imdbId}")
    MovieDetail getMovieDetailByImdbID(String imdbId);
}
