package me.miso.movieclient;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.androidannotations.annotations.EApplication;

import java.io.IOException;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import me.miso.movieclient.greendao.dao.DaoMaster;
import me.miso.movieclient.greendao.dao.DaoSession;
import me.miso.movieclient.greendao.dao.MovieDetailDao;
import me.miso.movieclient.greendao.dao.MovieListItemDao;
import me.miso.movieclient.greendao.dao.MovieSearchDao;
import me.miso.movieclient.model.MovieDetail;
import me.miso.movieclient.model.MovieListItem;
import me.miso.movieclient.model.MovieSearch;

/**
 * Created by miso on 18.09.2015.
 */

@EApplication
public class MovieApplication extends Application {

    private static Context context;

    //database connection / session related
    private SQLiteDatabase databaseConnection;
    private DaoMaster daoMaster;
    private DaoSession daoSession;

    private static MovieDetailDao daoMovieDetail;
    private static MovieListItemDao daoMovieListItem;
    private static MovieSearchDao daoMovieSearch;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        databaseConnection = new DaoMaster.DevOpenHelper(this, getString(R.string.DB_NAME), null)
                .getWritableDatabase();
        daoMaster = new DaoMaster(databaseConnection);
        daoSession = daoMaster.newSession(); // we can instantiate multiple sessions as well, sessions share the connection owned by the DaoMaster!
        daoMovieDetail = daoSession.getMovieDetailDao();
        daoMovieListItem = daoSession.getMovieListItemDao();
        daoMovieSearch= daoSession.getMovieSearchDao();
        //DaoMaster.dropAllTables(daoMaster.getDatabase(), true);
        DaoMaster.createAllTables(daoMaster.getDatabase(), true);
    }


    public static MovieSearchDao getDaoMovieSearch() {
        return daoMovieSearch;
    }

    public static MovieListItemDao getDaoMovieListItem() {
        return daoMovieListItem;
    }

    public static MovieDetailDao getDaoMovieDetail() {
        return daoMovieDetail;
    }

    public static Context getContext() {
        return context;
    }
}
