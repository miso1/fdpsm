package me.miso.movieclient.listener;

/**
 * Created by Phil on 20.09.15.
 */
public interface SearchListener {

    void onQuerySubmit(String searchQuery);

    void onQueryTextChanged(String searchQuery);

}
