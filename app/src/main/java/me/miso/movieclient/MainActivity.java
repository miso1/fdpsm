package me.miso.movieclient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import me.miso.movieclient.model.Broadcast;
import me.miso.movieclient.model.MovieSearch;

@EActivity(R.layout.main_activity)
public class MainActivity extends AppCompatActivity
        implements MovieListFragment.Callbacks, FragmentManager.OnBackStackChangedListener, QueryCacheFragment.Callbacks {

    private boolean isdetailView = false;
    private String query;

    @ViewById(R.id.movie_toolbar)
    Toolbar toolbar;
    private boolean canback;

    @ViewById(R.id.detail_container)
    FrameLayout detailView;

    QueryCacheFragment queryCacheFragment;

    @AfterViews
    void initActivity() {
        setSupportActionBar(toolbar);
        canback = getSupportFragmentManager().getBackStackEntryCount() > 0;
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main_activity);

        Toast.makeText(getBaseContext(), String.valueOf(getResources().getBoolean(R.bool.dual_pane)), Toast.LENGTH_SHORT).show();

        //Listen for changes in the back stack
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        //Handle when activity is recreated like on orientation Change
        shouldDisplayHomeUp();
        if (savedInstanceState == null) {
            queryCacheFragment = new QueryCacheFragment_();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, queryCacheFragment)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        menuItem.setVisible(!isdetailView);
        menuItem.setEnabled(!isdetailView);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint(getString(R.string.query_hint));
        if (query != null && !isdetailView) {
            menuItem.expandActionView();
            searchView.setQuery(query, false);
        }
        toolbar.setTitle(R.string.app_name);
        searchView.clearFocus();

        //TODO Configure the search info and add any event listeners
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Bundle args = new Bundle();

                args.putString("query", query);
                MovieListFragment_ movieListFragment = new MovieListFragment_();
                movieListFragment.setArguments(args);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, movieListFragment)
                        .commit();
                //MainActivity.this.query = query;
                //Intent intent = new Intent(Broadcast.search).putExtra(Broadcast.Extras.query, query);
                //LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Intent intent = new Intent(Broadcast.searchChanged).putExtra(Broadcast.Extras.id, query);
                LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                //Toast.makeText(this, "TEST", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp() {
        //Enable Up button only  if there are entries in the back stack
        canback = getSupportFragmentManager().getBackStackEntryCount() > 0;
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        isdetailView = false;
        invalidateOptionsMenu();
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Callback method from {@link MovieListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(String id) {
        Bundle arguments = new Bundle();
        this.isdetailView = true;
        invalidateOptionsMenu();

        MovieDetailFragment fragment = new MovieDetailFragment_();
        arguments.putString(MovieDetailFragment.ARG_IMDB_ID, id);
        fragment.setArguments(arguments);
        if (detailView == null) {
            getSupportFragmentManager().beginTransaction().addToBackStack(id)
                    .replace(R.id.fragment_container, fragment)
                    .commit();
        } else {
            // DisplayFragment (Fragment B) is in the layout (tablet layout),
            // so tell the fragment to update
            getSupportFragmentManager().beginTransaction().addToBackStack(id)
                    .replace(R.id.detail_container, fragment)
                    .commit();
        }

    }

    @Override
    public void onItemSelected(MovieSearch model) {
        this.query = model.getQueryText();

        Bundle args = new Bundle();
        args.putString("query", this.query);
        args.putBoolean("db", true);
        MovieListFragment_ movieListFragment = new MovieListFragment_();
        movieListFragment.setArguments(args);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, movieListFragment)
                .commit();
        //Intent intent = new Intent(Broadcast.search).putExtra(Broadcast.Extras.query, query);
        //LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
    }
}
