package me.miso.movieclient;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import me.miso.movieclient.greendao.dao.MovieDetailDao;
import me.miso.movieclient.model.Broadcast;
import me.miso.movieclient.model.MovieDetail;
import me.miso.movieclient.restclient.MovieClient;
import me.miso.movieclient.restclient.RestClient;
import me.miso.movieclient.util.Mapper;

@EFragment(R.layout.fragment_movie_detail)
public class MovieDetailFragment extends android.support.v4.app.Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_IMDB_ID = "item_id";

    private MovieDetail detail;

    @ViewById(R.id.movie_detail_frag)
    View rootView;

    @ViewById(R.id.title)
    TextView titleView;

    @ViewById(R.id.yearOfRelease)
    TextView yearView;

    @ViewById(R.id.runtime)
    TextView runtimeView;

    @ViewById(R.id.poster)
    ImageView posterView;

    @ViewById(R.id.loadingPanel)
    View loadingPanleView;

    @RestService
    MovieClient movieClient;

    private Context context;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MovieDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = MovieApplication.getContext();
        setRetainInstance(true);
        if (getArguments().containsKey(ARG_IMDB_ID)) {
            getMovieDetail();
        }
    }

    @Background
    void getMovieDetail() {
        me.miso.movieclient.greendao.dao.MovieDetail unique = MovieApplication.getDaoMovieDetail().queryBuilder().where(MovieDetailDao.Properties.ImdbID.eq(getArguments().getString(ARG_IMDB_ID))).unique();

        if (unique == null) {
            detail = movieClient.getMovieDetailByImdbID(getArguments().getString(ARG_IMDB_ID));
        } else {
            detail = Mapper.movieDetailDBMapToMovieDetail(unique);
        }
        Intent intent = new Intent(Broadcast.response_detail);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return rootView;
    }

    @Receiver(actions = Broadcast.response_detail, local = true)
    void initView() {
        if (detail != null) {
            titleView.setText(detail.getTitle());
            yearView.setText(detail.getYear());
            runtimeView.setText(detail.getRuntime());
            if (detail.getBitmapPoster() == null) {
                RestClient.getInstance().getBitmapFromURL(detail.getPoster(), new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        posterView.setImageBitmap(response);
                        detail.setBitmapPoster(response);
                        MovieApplication.getDaoMovieDetail().insertOrReplace(Mapper.movieDetailMapToMovieDetailDB(detail));
                        loadingPanleView.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadingPanleView.setVisibility(View.GONE);
                        MovieApplication.getDaoMovieDetail().insertOrReplace(Mapper.movieDetailMapToMovieDetailDB(detail));
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                posterView.setImageBitmap(detail.getBitmapPoster());
                loadingPanleView.setVisibility(View.GONE);
            }

        }
    }
}
