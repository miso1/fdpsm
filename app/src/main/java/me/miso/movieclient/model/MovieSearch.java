package me.miso.movieclient.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by miso on 18.09.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MovieSearch {

    @JsonIgnore
    private Long id;

    @JsonIgnore
    private String queryText;

    @JsonProperty("Search")
    private List<MovieListItem> search;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQueryText() {
        return queryText;
    }

    public void setQueryText(String queryText) {
        this.queryText = queryText;
    }

    public List<MovieListItem> getSearch() {
        return search;
    }

    public void setSearch(List<MovieListItem> search) {
        this.search = search;
    }

    @Override
    public String toString() {
        return queryText;
    }
}
