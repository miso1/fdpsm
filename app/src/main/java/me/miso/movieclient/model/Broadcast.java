package me.miso.movieclient.model;

/**
 * Created by miso on 29.09.2015.
 */
public interface Broadcast {
    String search = "me.miso.search";
    String searchChanged = "me.miso.searchChanged";
    String response_list = "me.miso.response_list";
    String response_detail = "me.miso.response_detail";

    public interface Extras {
        String query = "me.miso.searchTerm";
        String id = "me.miso.imdbId";
    }
}
