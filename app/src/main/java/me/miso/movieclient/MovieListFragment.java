package me.miso.movieclient;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.common.collect.Lists;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.List;

import me.miso.movieclient.greendao.dao.MovieSearchDao;
import me.miso.movieclient.model.Broadcast;
import me.miso.movieclient.model.MovieListItem;
import me.miso.movieclient.model.MovieSearch;
import me.miso.movieclient.restclient.MovieClient;
import me.miso.movieclient.util.Mapper;

/**
 * A list fragment representing a list of Movies. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link MovieDetailFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */

@EFragment(R.layout.fragment_movie_list)
public class MovieListFragment extends android.support.v4.app.Fragment {

    private static List<MovieListItem> cache = Lists.newArrayList();

    private Callbacks mCallbacks = sDummyCallbacks;

    private ArrayAdapter<MovieListItem> movieListItemArrayAdapter;

    @ViewById(R.id.movie_list)
    ListView listView;

    @ViewById(R.id.movie_list)
    View rootView;


    @RestService
    MovieClient movieClient;
    private MovieSearch response;

    @UiThread
    public void onResponse() {
        movieListItemArrayAdapter.clear();
        movieListItemArrayAdapter.addAll(response.getSearch() == null ? Lists.<MovieListItem>newArrayList() : response.getSearch());
        cache = response.getSearch() == null ? Lists.<MovieListItem>newArrayList() : response.getSearch();
        listView.setVisibility(View.VISIBLE);
    }

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(String id);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(String id) {
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MovieListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        movieListItemArrayAdapter = new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1);
    }

    @AfterViews
    void init() {
        listView.setAdapter(movieListItemArrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onListItemClick(view, position, id);
            }
        });
    }

    @Background
    public void searchMovies(String query) {
        movieListItemArrayAdapter.clear();
        response = movieClient.searchMovies(query);
        response.setQueryText(query);
        me.miso.movieclient.greendao.dao.MovieSearch entity = Mapper.movieSearchMapToMovieSearchDB(response);
        long insert = MovieApplication.getDaoMovieSearch().insert(entity);
        for (MovieListItem listItem : response.getSearch()) {
            me.miso.movieclient.greendao.dao.MovieListItem entity1 = Mapper.movieListItemMapToMovieListItemDB(listItem, insert);
            MovieApplication.getDaoMovieListItem().insertOrReplace(entity1);
            entity.getMovieListItemList().add(entity1);
        }
        entity.update();
        onResponse();
    }

    @Receiver(actions = Broadcast.searchChanged, local = true)
    public void onSearchChanged(@Receiver.Extra((Broadcast.Extras.id)) String imdbId) {
        listView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);
        if (container != null) {
            container.removeView(view);
        }
        movieListItemArrayAdapter.addAll(cache);

        String query = getArguments().getString("query");
        if (getArguments().getBoolean("db")) {
            response = Mapper.movieSearchDBMapToMovieSearch(MovieApplication
                    .getDaoMovieSearch().queryBuilder()
                    .where(MovieSearchDao.Properties.QueryText.eq(query)).limit(1).unique());
            onResponse();
        } else {
            searchMovies(query);

        }
        return view;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    public void onListItemClick(View view, int position, long id) {
        //super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onItemSelected(movieListItemArrayAdapter.getItem(position).getImdbID());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /*
      The current activated item position. Only used on tablets.
     */
        int mActivatedPosition = ListView.INVALID_POSITION;
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            //outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }
}
