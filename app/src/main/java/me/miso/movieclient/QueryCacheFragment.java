package me.miso.movieclient;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.common.collect.Lists;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import me.miso.movieclient.model.MovieSearch;
import me.miso.movieclient.util.Mapper;

/**
 * A list fragment representing a list of Movies. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link MovieDetailFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */

@EFragment(R.layout.fragment_query_cache)
public class QueryCacheFragment extends android.support.v4.app.Fragment {

    private static List<MovieSearch> cache = Lists.newArrayList();

    private Callbacks mCallbacks = sDummyCallbacks;

    private ArrayAdapter<MovieSearch> queryCacheResultArrayAdapter;

    @ViewById(R.id.cache_list)
    ListView listView;

    @ViewById(R.id.query_cache_frag)
    View rootView;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(MovieSearch model);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(MovieSearch model) {
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public QueryCacheFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        queryCacheResultArrayAdapter = new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1);
        for (me.miso.movieclient.greendao.dao.MovieSearch search : MovieApplication.getDaoMovieSearch().loadAll()) {
            cache.add(Mapper.movieSearchDBMapToMovieSearch(search));
        }
    }

    @AfterViews
    void init() {
        listView.setAdapter(queryCacheResultArrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onListItemClick(view, position, id);
            }
        });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_query_cache, container, false);
        if (container != null) {
            container.removeView(view);
        }
        queryCacheResultArrayAdapter.addAll(cache);
        super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    public void onListItemClick(View view, int position, long id) {
        //super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onItemSelected(queryCacheResultArrayAdapter.getItem(position));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /*
      The current activated item position. Only used on tablets.
     */
        int mActivatedPosition = ListView.INVALID_POSITION;
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            //outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }
}
